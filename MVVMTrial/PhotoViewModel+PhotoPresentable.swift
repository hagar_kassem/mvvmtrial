//
//  PostViewModel+BodyLabelPresentable.swift
//  MVVMTrial
//
//  Created by Hager on 3/16/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit


protocol PhotoPresentable {
    
    var photoLink: String { get }    
}
extension PhotoViewModel: PhotoPresentable {
    
    var photoLink: String {return photos.map{$0.photoLink as String!}[PhotoViewModel.sharedInstance().index]}
}

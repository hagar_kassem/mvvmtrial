//
//  DataManager.swift
//  MVVMTrial
//
//  Created by Hager on 3/16/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit
import Alamofire
import Gloss

protocol RequestFormer {
    
}

class DataManager {
    
    static private var instance = DataManager()
    private var apiClient = APIClient()
    
    static func sharedInstance() -> DataManager {
        return instance
    }
    
    public func requestPostArrayData(FromURLWithString url: String,
                                     successHandler: @escaping ([Any])->Void,
                                     failureHandler: @escaping (Error)->Void) {
        
        if let validRequest = requestWithString(requestURLString: url) {
            
            apiClient.sendRequest(ToWebService: validRequest,
                                  successHandler: { (dataArray: DataResponse<Any>) in
                                    //MARK:- Parse Posts
                                    guard let posts = [Post].from(jsonArray: dataArray.result.value as! [JSON]) else {
                                        debugPrint("Deserilization Failed")
                                        return
                                    }
                                    debugPrint("Deserilization Succeed")
                                    successHandler(posts)

            },
                                  failureHandler: { (error: Error) in
                                    debugPrint("Invalid Request with error: \(error)")
                                    failureHandler(error)
            })
            
        }
        else {
            
        }
    }

    
    public func requestPhotoArrayData(FromURLWithString url: String,
                                     successHandler: @escaping ([Any])->Void,
                                     failureHandler: @escaping (Error)->Void) {
        
        if let validRequest = requestWithString(requestURLString: url) {
            
            apiClient.sendRequest(ToWebService: validRequest,
                                  successHandler: { (dataArray: DataResponse<Any>) in
                                    //MARK:- Parse Photos
                                    guard let photos = [Photo].from(jsonArray: dataArray.result.value as! [JSON]) else {
                                            debugPrint("Deserilization Failed")
                                            return
                                        }
                                        debugPrint("Deserilization Succeed")
                                        successHandler(photos)
            },
                                  failureHandler: { (error: Error) in
                                    debugPrint("Invalid Request with error: \(error)")
                                    failureHandler(error)
            })
            
        }
        else {
            
        }
    }


}

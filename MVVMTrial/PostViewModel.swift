//
//  PostViewModel.swift
//  MVVMTrial
//
//  Created by Hager on 2/6/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit



class PostViewModel {
        
    var index: Int = 0
    static private var instance = PostViewModel()

    var posts: Array<Post> {return Utilities.posts.filter{$0.title != nil && $0.body != nil}}
    
    static func sharedInstance() -> PostViewModel {
        return instance
    }
 
    
}


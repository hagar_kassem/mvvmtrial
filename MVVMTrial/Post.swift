//
//  Post.swift
//  MVVMTrial
//
//  Created by Hager on 2/6/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit
import Gloss
import Alamofire

class Post :AbstractModel {
    //MARK: - Class Properties
    static let instance : Post = Post(json: ["":""])!
    
    var title: String!
    var body: String!
    
    //MARK: - Singleton
    public static func sharedInstance() -> Post {
        return instance
    }
    override init() {
        super.init()

    }
    //MARK: - Deserialization
    required init?(json: JSON) {
        super.init()
        self.title = "title" <~~ json
        self.body = "body" <~~ json
    }
    
}



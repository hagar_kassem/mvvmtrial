//
//  Photo.swift
//  MVVMTrial
//
//  Created by Hager on 2/6/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit
import Gloss
import Alamofire

class Photo :AbstractModel {
    
    //MARK: - Class Properties
    static let instance : Photo = Photo(photo: "")
    
    var photoLink: String?
    
    //MARK: - Singleton
    public static func sharedInstance() -> Photo {
        return instance
    }
    
    init(photo: String) {
        super.init()
        self.photoLink = photo
    }
    
    //MARK: - Deserialization
    required init?(json: JSON) {
        super.init()
        self.photoLink = "url" <~~ json
    }

}

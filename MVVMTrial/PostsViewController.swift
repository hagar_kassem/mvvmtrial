//
//  PostsTableViewController.swift
//  MVVMTrial
//
//  Created by Hager on 2/6/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit

class PostsViewController: UITableViewController {

    static var postsCount: Int = 0
    var imagesVC: PhotosViewController!
    
    func updateTableView(notification: NSNotification) {
        PostsViewController.postsCount = (notification.object as? Int)!
        tableView.reloadData()
    }
    
    @IBAction func showImages(_ sender: Any) {
        imagesVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PhotosViewController") as! PhotosViewController
        self.navigationController?.pushViewController(imagesVC, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        requestPosts()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateTableView),
                                               name: NSNotification.Name(rawValue: "postsLoaded"),
                                               object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PostsViewController.postsCount
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        PostViewModel.sharedInstance().index = indexPath.row
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostCell
        // Configure the cell...
        cell.configurePostCell(WithDelegate: PostViewModel.sharedInstance())
        return cell
        
    }
    
    //MARK:- Request Posts
    func requestPosts() {
        DataManager
            .sharedInstance()
            .requestPostArrayData(FromURLWithString: Webservice.posts,
                              successHandler: { (posts: [Any]) in
                                Utilities.sharedInstance().store(Elements: posts, OfType: Post.self)
                                Utilities.sharedInstance().postNotification(WithName: "postsLoaded", AndCount: posts.count)
                                
                            }) { (error: Error) in
                                debugPrint(error)
                            }
    }



}


//
//  PostViewModel+TitleLabelPresentable.swift
//  MVVMTrial
//
//  Created by Hager on 3/16/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit

protocol TitleLabelPresentable {
    
    var titleText: String {get}
    var titleTextColor: UIColor{get}
    var titleFont: UIFont {get}
}
extension PostViewModel: TitleLabelPresentable {
    
    var titleTextColor:UIColor {return .green}
    var titleText: String {return posts.map {$0.title as String!}[PostViewModel.sharedInstance().index]}
    var titleFont: UIFont {return .preferredFont(forTextStyle: .title1)}
}

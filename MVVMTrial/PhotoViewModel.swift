//
//  PhotoViewModel.swift
//  MVVMTrial
//
//  Created by Hager on 2/6/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit



class PhotoViewModel {

    var index: Int = 0
    static var selectedIndex: Int = 0
    static private var instance = PhotoViewModel()
    
    var photos: Array<Photo> {return Utilities.photos.filter{$0.photoLink != nil}}
    
    static func sharedInstance() -> PhotoViewModel {
        return instance
    }
}

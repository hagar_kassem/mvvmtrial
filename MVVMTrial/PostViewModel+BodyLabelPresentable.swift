//
//  PostViewModel+BodyLabelPresentable.swift
//  MVVMTrial
//
//  Created by Hager on 3/16/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit

protocol BodyLabelPresentable {
    
    var bodyText: String {get}
    var bodyTextColor: UIColor{get}
    var bodyFont: UIFont {get}
    
}
extension PostViewModel: BodyLabelPresentable {

    var bodyTextColor:UIColor {return .blue}
    var bodyText: String {return posts.map {$0.body as String!}[PostViewModel.sharedInstance().index]}
    var bodyFont: UIFont {return .preferredFont(forTextStyle: .body)}
}

//
//  PhotoView+Previewable.swift
//  MVVMTrial
//
//  Created by Hager on 3/16/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit

protocol Previewable {
    
    func loadImageViewWithPhoto(photo: String)
    func currentPhotoIndex() -> Int
    func previousImageIndex() -> Int
    func nextPhotoIndex() -> Int
}

extension PhotoView: Previewable {
    func currentPhotoIndex() -> Int {
        return Utilities.photos.index(where: {$0.photoLink == Utilities.currentPhoto.photoLink})!
    }
    
    func previousImageIndex() -> Int {
        return currentPhotoIndex() == 0 ? Utilities.photos.count-1 : currentPhotoIndex()-1
    }
    
    func nextPhotoIndex() -> Int {
        return currentPhotoIndex() == Utilities.photos.count-1 ? 0 : currentPhotoIndex()+1
    }
}

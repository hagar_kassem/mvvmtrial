//
//  Utilities.swift
//  MVVMTrial
//
//  Created by Hager on 2/8/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit

class Utilities {
    
    static private var instance = Utilities()
    
    static var posts: Array<Post>!
    static var photos: Array<Photo>!
    static var currentPhoto: Photo!
    
    static func sharedInstance() -> Utilities {
        return instance
    }
    
    func store(Elements elements: [Any] ,
                       OfType type: AbstractModel.Type) {
        if type is Post.Type {
            Utilities.posts = elements as! [Post]
        }
        else if type is Photo.Type {
            Utilities.photos = elements as! [Photo]
        }
        
    }
    func postNotification(WithName name: String,
                                  AndCount count: Int) {
        NotificationCenter.default.post(
            name: NSNotification.Name(rawValue: name),
            object: count)
    }
}

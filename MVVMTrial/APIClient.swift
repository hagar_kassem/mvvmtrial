//
//  APIClient.swift
//  MVVMTrial
//
//  Created by Hager on 3/16/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit
import Alamofire

class APIClient {
    
    func sendRequest(ToWebService webService: URLRequest?,
                 successHandler: @escaping (DataResponse<Any>)->Void,
                 failureHandler: @escaping (Error)->Void) {
        
        Alamofire
            .request(webService!)
            .validate()
            .responseJSON {
                (response: DataResponse<Any>) -> Void in
                switch response.result {
                case .success:
                    successHandler(response)
                case .failure(let error):
                    failureHandler(error)
                }
        }
    }
    
}

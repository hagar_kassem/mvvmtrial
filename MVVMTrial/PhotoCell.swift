//
//  PhotoCell.swift
//  MVVMTrial
//
//  Created by Hager on 2/6/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit
import Kingfisher

class PhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    
    func configurePhotoCell(WithDelegate delegate: PhotoViewModel) {
        let url = URL(string: delegate.photoLink)
        photoImageView.kf.setImage(with: url)
        
    }
    
}

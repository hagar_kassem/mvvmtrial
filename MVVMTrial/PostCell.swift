//
//  PostCell.swift
//  MVVMTrial
//
//  Created by Hager on 2/6/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit

typealias PostCellProtocol = TitleLabelPresentable & BodyLabelPresentable
class PostCell : UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    
    func configurePostCell(WithDelegate delegate: PostCellProtocol) {
    
        titleLabel.text = delegate.titleText
        titleLabel.font = delegate.titleFont
        titleLabel.textColor = delegate.titleTextColor
        
        bodyLabel.text = delegate.bodyText
        bodyLabel.font = delegate.bodyFont
        bodyLabel.textColor = delegate.bodyTextColor
        
    }
    
}

//
//  PhotosViewController.swift
//  MVVMTrial
//
//  Created by Hager on 2/6/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit

private let reuseIdentifier = "PhotoCell"

class PhotosViewController: UICollectionViewController {

    static var photosCount: Int = 0
    var photoVC: PhotoViewController!
    
    func updateCollectionView(notification: NSNotification) {
        PhotosViewController.photosCount = (notification.object as? Int)!
        collectionView?.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        requestPhotos()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateCollectionView),
                                               name: NSNotification.Name(rawValue: "photosLoaded"),
                                               object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return PhotosViewController.photosCount
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        PhotoViewModel.sharedInstance().index = indexPath.row
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PhotoCell
        // Configure the cell...
        cell.configurePhotoCell(WithDelegate: PhotoViewModel.sharedInstance())
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        // move your data order
        //        installsStandardGestureForInteractiveMovement = true by default for UICollectionViewController , so this method is called once included
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        PhotoViewModel.sharedInstance().index = indexPath.row
        photoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PhotoViewController") as! PhotoViewController
        let photoView: PhotoView = photoVC.view as! PhotoView
        photoView.configurePhotoView(WithDelegate: PhotoViewModel.sharedInstance())
        self.navigationController?.pushViewController(photoVC, animated: true)
        
    }
    
    //MARK:- Request Photos
    func requestPhotos() {
        DataManager.sharedInstance().requestPhotoArrayData(FromURLWithString: Webservice.photos,
                                                      successHandler: { (photos: [Any]) in
                                                        Utilities.sharedInstance().store(Elements: photos, OfType: Photo.self)
                                                        Utilities.sharedInstance().postNotification(WithName: "photosLoaded", AndCount: photos.count)
        }) { (error: Error) in
            debugPrint(error)
        }
    }

}

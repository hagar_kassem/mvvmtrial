//
//  PhotoView.swift
//  MVVMTrial
//
//  Created by Hager on 2/8/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit

class PhotoView: UIView {

    @IBOutlet weak var photoImageView: UIImageView!
    
    func configurePhotoView(WithDelegate delegate: PhotoPresentable) {
        loadImageViewWithPhoto(photo: delegate.photoLink)
        Utilities.currentPhoto = Photo(photo: delegate.photoLink)
    }
    
    @IBAction func showPreviousPhoto(_ sender: Any) {
        let previousImage = Utilities.photos[previousImageIndex()]
        loadImageViewWithPhoto(photo: previousImage.photoLink!)
        Utilities.currentPhoto = previousImage
    }
    
    @IBAction func showNextPhoto(_ sender: Any) {
        let nextImage = Utilities.photos[nextPhotoIndex()]
        loadImageViewWithPhoto(photo: nextImage.photoLink!)
        Utilities.currentPhoto = nextImage
    }
    
    func loadImageViewWithPhoto(photo: String) {
        let url = URL(string: photo)
        photoImageView.kf.setImage(with: url)
    }
    
}

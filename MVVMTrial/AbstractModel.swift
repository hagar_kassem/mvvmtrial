//
//  AbstractModel.swift
//  MVVMTrial
//
//  Created by Hager on 3/16/17.
//  Copyright © 2017 Hager. All rights reserved.
//

import UIKit
import Gloss

class AbstractModel: Decodable {
    init() {}
    required init?(json: JSON) {
    }
}

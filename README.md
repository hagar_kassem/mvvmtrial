MVVM Design Pattern

* MVVM stands for Model-View-ViewModel 

Model: 
* Consist of : 
- Object Bean (Properties & Setters/Getters). 
- API Client (Access API via requests).
- Data Manager (Return parsed data according to passed model).

View:
* Consist of view properties & view configuration method. 

ViewModel:
* Act as adapter for Model to be presented on View.
* Make use of extensions to isolate presenting of view items (Protocol-Oriented-Programming)

ViewController 	Functionalities :
* Request parsed data via DataManager sharedInstance.
* Configure View by related ViewModel.